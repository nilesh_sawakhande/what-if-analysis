import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatCardModule} from '@angular/material/card';
import { MatSliderModule } from '@angular/material/slider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDividerModule} from '@angular/material/divider';
import {MatTableModule} from '@angular/material/table';

import { FormatPipe } from '../pipes/format.pipe';
import { PercentPipe } from '../pipes/percent.pipe';

const exportedComponents = [
  MatCardModule,
  MatSliderModule,
  MatExpansionModule,
  MatDividerModule,
  MatTableModule,
  FormatPipe,
  PercentPipe,
]

@NgModule({
  declarations: [
    FormatPipe,
    PercentPipe,
  ],
  imports: [
    CommonModule,
  ],
  exports: [...exportedComponents]
})
export class CoreModule { }
