import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { ContainerComponent } from './container/container.component';

const routes: Routes = [
  {
    path: 'dashboard', component: ContainerComponent,
    children: [
      {
        path:'what-if', loadChildren: () => import('./what-if-analysis/what-if-analysis.module').then(m => m.WhatIfAnalysisModule)
      }
    ]
  },
  { path: '', redirectTo: '/dashboard/what-if', pathMatch: 'full' },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
