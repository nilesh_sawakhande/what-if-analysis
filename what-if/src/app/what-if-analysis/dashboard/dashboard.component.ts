import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';

export interface BranchData {
  branchName: string;
  baseQuantity: number;
  basePrice: number;
  revenue?: number;
  cogs?: number;
  gp?: number;
  gpPer?: number;
  totalCost?: number;
  np?: number;
  npPer?: number;
}

export interface VarianceData {
  revenue?: number;
  cogs?: number;
  gp?: number;
  gpPer?: number;
  totalCost?: number;
  np?: number;
  npPer?: number;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  data: Array<BranchData> = [
    {
      branchName: 'Ahmedabad Branch',
      baseQuantity: 3628,
      basePrice: 18724,
      totalCost: 25945724.55
    },
    {
      branchName: 'Bangalore Branch',
      baseQuantity: 7500,
      basePrice: 18724,
      totalCost: 25945724.55
    },
    {
      branchName: 'Delhi Branch',
      baseQuantity: 7300,
      basePrice: 18724,
      totalCost: 25945724.55
    },
    {
      branchName: 'Head Office',
      baseQuantity: 7100,
      basePrice: 60724,
      totalCost: 123551069
    }
  ]

  tempData: Array<BranchData> = []
  total: BranchData = {
    basePrice: 0, baseQuantity: 0, branchName: '', cogs: 0, gp: 0, gpPer: 0, np: 0, npPer: 0, revenue: 0, totalCost: 0
  };
  tempTotal: BranchData = {
    basePrice: 0, baseQuantity: 0, branchName: '', cogs: 0, gp: 0, gpPer: 0, np: 0, npPer: 0, revenue: 0, totalCost: 0
  };
  totalVariance: VarianceData = {
    cogs: 0, gp: 0, gpPer: 0, np: 0, npPer: 0, revenue: 0, totalCost: 0
  };
  variance = 0;
  percentage = 0;
  constructor(private service: DataService) { }


  ngOnInit() {
    this.calcActuals(this.data)
  }

  calcActuals(data: Array<BranchData>) {
    let newData = data.map((item: any) => {
      let rev = item.basePrice * item.baseQuantity;
      let cog = rev * (60 / 100);
      let gp = rev - cog;
      let gpPercent = Math.round((gp / rev) * 100);
      let np = gp - item.totalCost;
      let npPercent = Math.round((np / rev) * 100);
      return {
        branchName: item.branchName,
        baseQuantity: item.baseQuantity,
        basePrice: item.basePrice,
        revenue: rev,
        cogs: cog,
        gp: gp,
        gpPer: gpPercent,
        totalCost: item.totalCost,
        np: np,
        npPer: npPercent
      }
    })
    this.data = newData
    this.tempData = this.data.slice()
    this.calcTotal()

    // console.log("Actuals: ", this.data);
    // console.log("Temp: ", this.tempData);
  }

  onBranchData(dat: { data: BranchData, index: number }) {
    const { data, index } = dat;
    // console.log('data from slider: ',data);
    this.tempData[index] = data
    this.calcTotalTemp()
  }

  calcTotal() {
    this.total!.revenue = this.data.reduce((a, b) => a + b.revenue!, 0)
    this.total!.cogs = this.data.reduce((a, b) => a + b.cogs!, 0)
    this.total!.gp = this.data.reduce((a, b) => a + b.gp!, 0)
    this.total!.gpPer = this.data.reduce((a, b) => a + Math.round(b.gpPer!), 0) / this.data.length
    this.total!.totalCost = this.data.reduce((a, b) => a + b.totalCost!, 0)
    this.total!.np = this.data.reduce((a, b) => a + b.np!, 0)
    this.total!.npPer = this.data.reduce((a, b) => a + Math.round(b.npPer!), 0) / this.data.length
    // console.log(totalData);
    this.tempTotal = { ...this.total }
    this.variance = this.tempTotal.revenue! - this.total.revenue!
  }

  calcTotalTemp() {
    this.tempTotal.revenue = this.tempData.reduce((a, b) => a + b.revenue!, 0)
    this.tempTotal.cogs = this.tempData.reduce((a, b) => a + b.cogs!, 0)
    this.tempTotal.gp = this.tempData.reduce((a, b) => a + b.gp!, 0)
    this.tempTotal.gpPer = this.tempData.reduce((a, b) => a + Math.round(b.gpPer!), 0) / this.tempData.length
    this.tempTotal.totalCost = this.tempData.reduce((a, b) => a + b.totalCost!, 0)
    this.tempTotal.np = this.tempData.reduce((a, b) => a + b.np!, 0)
    this.tempTotal.npPer = this.tempData.reduce((a, b) => a + Math.round(b.npPer!), 0) / this.tempData.length
    // this.variance = this.tempTotal.revenue! - this.total.revenue!
    // this.percentage = (this.variance / this.total.revenue!) * 100

    this.totalVariance.revenue = ((this.tempTotal.revenue! - this.total.revenue!)/this.total.revenue!)*100
    this.totalVariance.cogs = ((this.tempTotal.cogs! - this.total.cogs!)/this.total.cogs!)*100
    this.totalVariance.gp = ((this.tempTotal.gp! - this.total.gp!)/this.total.gp!)*100
    this.totalVariance.gpPer = this.tempTotal.gpPer! - this.total.gpPer!
    this.totalVariance.totalCost = ((this.tempTotal.totalCost! - this.total.totalCost!)/this.total.totalCost!)*100
    this.totalVariance.np = ((this.tempTotal.np! - this.total.np!)/this.total.np!)*100
    this.totalVariance.npPer = this.tempTotal.npPer! - this.total.npPer!
  }

  cellClassDefault(index: number, field: keyof BranchData) {
    let actualVal = Math.round(Number(this.data![index][field]));
    let val = Math.round(Number(this.tempData![index][field]));
    if (actualVal == val) {
      return true;
    }
    else {
      return false;
    }
  }

  cellClassSuccess(index: number, field: keyof BranchData) {
    let actualVal = Math.round(Number(this.data![index][field]));
    let val = Math.round(Number(this.tempData![index][field]));
    if (actualVal! < val!) {
      return true;
    }
    else {
      return false;
    }
  }

  cellClassDanger(index: number, field: keyof BranchData) {
    let actualVal = Math.round(Number(this.data![index][field]));
    let val = Math.round(Number(this.tempData![index][field]));
    
    if (actualVal! > val!) {
      return true;
    }
    else {
      return false;
    }
  }

  varCellClass(value: number){
    let val = Math.round(value)
    if(val < 0){
      return 'table-danger'
    }
    else if(val > 0){
      return 'table-success'
    }
    else{
      return 'table-default'
    }
  }

  onResetAll() {
    this.calcActuals(this.data)
    this.variance = 0;
    this.percentage = 0
  }
}
