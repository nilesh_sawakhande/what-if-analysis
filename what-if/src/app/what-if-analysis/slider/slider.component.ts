import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

export interface BranchData {
  branchName: string;
  baseQuantity: number;
  basePrice: number;
  revenue?: number;
  cogs?: number;
  gp?: number;
  gpPer?: number;
  totalCost?: number;
  np?: number;
  npPer?: number;
}

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {
  @Input() data: BranchData | undefined;
  @Input() index: number | undefined;
  @Output() calcData = new EventEmitter<{data: BranchData, index: number}>()
  tempData: BranchData | undefined;
  baseQuanTimeout: any;
  basePriceTimeout: any;
  constructor() { }

  ngOnInit(): void {
    this.calcActuals(this.data)
  }
  qntSlider: number = 0;
  priceSlider: number = 0;

  formatLabel(value: number) {
    return value + '%';
  }

  calcActuals(data: BranchData | undefined) {
    let newData = (data: any) => {
      let rev = data.basePrice * data.baseQuantity;
      let cog = rev * (60/100);
      let gp = rev - cog;
      let gpPercent = Math.round((gp/rev) * 100);
      let np = gp - data.totalCost;
      let npPercent = Math.round((np/rev) * 100);
      return {
        branchName: data.branchName,
        baseQuantity: data.baseQuantity,
        basePrice: data.basePrice,
        revenue: rev,
        cogs: cog,
        gp: gp,
        gpPer: gpPercent,
        totalCost: data.totalCost,
        np: np,
        npPer: npPercent
      }
    }
    this.data = newData(data)
    this.tempData = this.data
    this.calcData.emit({data: this.tempData, index: this.index!})
    // //console.log("Actuals: ", this.data);
  }

  calcOnTempChange(quantity:any, price: any){
    let data = this.data
    let newData = () => {
      let rev = price * quantity;
      let cog = (this.data!.cogs!/this.data!.baseQuantity) * quantity;
      let gp = rev - cog;
      let gpPercent = Math.round((gp/rev) * 100);
      //console.log(this.prevTotalCost, this.prevRevenue, rev);
      let totalCst = (data!.totalCost!/data!.revenue!) * rev
      let np = gp - totalCst!;
      let npPercent = Math.round((np/rev) * 100);
      return {
        branchName: data!.branchName,
        baseQuantity: quantity,
        basePrice: price,
        revenue: rev,
        cogs: cog,
        gp: gp,
        gpPer: gpPercent,
        totalCost: totalCst,
        np: np,
        npPer: npPercent
      }
    }
    this.tempData = newData()
    this.calcData.emit({data: this.tempData, index: this.index!})
  }

  calcQntyOnChange(change: any) {
    this.qntSlider = change
    if(this.baseQuanTimeout) clearTimeout(this.baseQuanTimeout)
    this.baseQuanTimeout = setTimeout(() => {
      const quantity = Math.round(this.data!.baseQuantity * (1 + (change / 100)))
      const price = Math.round(this.data!.basePrice * (1 + (this.priceSlider / 100)))
      this.calcOnTempChange(quantity, price)
    }, 1000);
  }
  calcPriceOnChange(change: any) {
    this.priceSlider = change
    if(this.basePriceTimeout) clearTimeout(this.basePriceTimeout)
    this.basePriceTimeout = setTimeout(() => {
      const quantity = Math.round(this.data!.baseQuantity * (1 + this.qntSlider / 100))
      const price = Math.round(this.data!.basePrice * (1 + (change / 100)))
      this.calcOnTempChange(quantity, price)
    }, 1000);
  }

  onReset(){
    if(this.baseQuanTimeout) clearTimeout(this.baseQuanTimeout)
    if(this.basePriceTimeout) clearTimeout(this.basePriceTimeout)
    this.qntSlider = 0,
    this.priceSlider = 0
    this.calcActuals(this.data)
  }  
}
