import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard/dashboard.component';
import { SliderComponent } from './slider/slider.component';

import { WhatIfRoutingModule } from './what-if-routing.module';
import { CoreModule } from '../core-module/core-module.module';

const exports: any = [
  DashboardComponent,
  SliderComponent]

@NgModule({
  declarations: [
    DashboardComponent,
    SliderComponent
  ],
  imports: [
    CommonModule,
    WhatIfRoutingModule,
    CoreModule
  ],
  exports: [...exports]
})
export class WhatIfAnalysisModule { }
